class State1 {
    demos: any[];
    newDemo: any;

    constructor(public Demo) {
        this.demos = Demo.query();
        this.newDemo = {};
    }

    addDemo() {
        this.Demo.save(this.newDemo, (data) => {
            this.demos.push(data);
        });
        this.newDemo = {};
    }

    removeDemo(demo) {
        this.Demo.delete({id: demo._id}, (data) => {
            let index = this.demos.indexOf(demo);
            this.demos.splice(index, 1);
        });
    }

    updateDemo(demo) {
        this.Demo.update({id: demo._id}, demo, (data) => {
            let index = this.demos.indexOf(demo);
            this.demos[index] = demo;
        });
    }
}

angular.module("tsfs").controller("State1", State1);
