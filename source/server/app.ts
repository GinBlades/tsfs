/// <reference path="../../typings/main.d.ts" />
import path = require("path");
import express = require("express");
import bodyParser = require("body-parser");
// import cors = require("cors");
import mongoose = require("mongoose");
import { IndexController } from "./routes/index";
import { ApiController } from "./routes/api_controller";

let app = express();
var config = require("./config.json");
import demo = require("./models/demo");

mongoose.connect(config.development.database, (err) => {
    if (err) {
        console.log("connection error", err);
    } else {
        console.log("connection successful");
    }
});

app.set("view engine", "jade");
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "../client")));

app.use("/", new IndexController().initialize());
app.use("/api/demos", new ApiController(<mongoose.Model<any>>demo).initialize());

app.listen(3000);
console.log("Express app running on port 3000");
module.exports = app;
