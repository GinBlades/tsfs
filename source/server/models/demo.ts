import mongoose = require("mongoose");

let DemoSchema = new mongoose.Schema({
    name: String,
    updated_at: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model("Demo", DemoSchema);
