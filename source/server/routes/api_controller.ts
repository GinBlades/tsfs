import express = require("express");
import mongoose = require("mongoose");

export class ApiController {
    router: express.Router;

    constructor(public model: mongoose.Model<any>) { }

    public initialize(): express.Router {
        this.router = express.Router();
        let actions = ["index", "show", "create", "update", "delete"];
        actions.forEach((action: string) => {
            this[action]();
        });
        return this.router;
    }

    public index() {
        this.router.get("/", (req, res, next) => {
            this.model.find(req.query, (err, records) => {
                if (err) {
                    return next(err);
                }
                return res.json(records);
            });
        });
    }

    public show() {
        this.router.get("/:id", (req, res, next) => {
            this.model.findById(req.params.id, (err, record) => {
                if (err) {
                    return next(err);
                }
                return res.json(record);
            });
        });
    }

    public create() {
        this.router.post("/", (req, res, next) => {
            this.model.create(req.body, (err, record) => {
                if (err) {
                    return next(err);
                }
                return res.json(record);
            });
        });
    }

    public update() {
        this.router.put("/:id", (req, res, next) => {
            this.model.findByIdAndUpdate(req.params.id, req.body, {new: true }, (err, record) => {
                if (err) {
                    return next(err);
                }
                return res.json(record);
            });
        });
    }

    public delete() {
        this.router.delete("/:id", (req, res, next) => {
            this.model.findByIdAndRemove(req.params.id, req.body, (err, record) => {
                if (err) {
                    return next(err);
                }
                return res.json(record);
            });
        });
    }
}
