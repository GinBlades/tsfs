import express = require("express");

export class IndexController {
    router: express.Router;

    public initialize(): express.Router {
        this.router = express.Router();
        let actions = ["home"];
        actions.forEach((action: string) => {
            this[action]();
        });
        return this.router;
    }

    public home() {
        return this.router.get("/", (req, res, next) => {
            res.render("index", {
                title: "Express",
                message: "Hello"
            });
        });
    }
}
