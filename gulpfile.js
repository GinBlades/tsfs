var gulp = require("gulp"),
    plumber = require("gulp-plumber"),
    sourceMaps = require("gulp-sourcemaps"),
    jade = require("gulp-jade"),
    ts = require("gulp-typescript"),
    tsLint = require("gulp-tslint"),
    concat = require("gulp-concat"),
    sass = require("gulp-sass"),
    config = require("./gulp/config.json");

gulp.task('tsc-server', function () {
	return gulp.src('source/server/**/*.ts')
        .pipe(plumber())
        .pipe(tsLint())
        .pipe(tsLint.report("prose", { emitError: false }))
		.pipe(ts({
            module: "commonjs"
        }))
		.pipe(gulp.dest('./build/server'));
});

gulp.task('tsc-client', function () {
	return gulp.src(config.clientJs)
        .pipe(plumber())
        .pipe(tsLint())
        .pipe(tsLint.report("prose", { emitError: false }))
        .pipe(sourceMaps.init())
		.pipe(ts())
        .pipe(concat("main.js"))
        .pipe(sourceMaps.write("."))
		.pipe(gulp.dest('./build/client/js'));
});

gulp.task("jade-server", function() {
    gulp.src("./source/server/**/*.jade")
        .pipe(plumber())
        .pipe(gulp.dest("./build/server"));
});

gulp.task("jade-client", function() {
    gulp.src("./source/client/**/*.jade")
        .pipe(plumber())
        .pipe(jade({doctype: "html"}))
        .pipe(gulp.dest("./build/client"));
});

gulp.task("jade", ["jade-client", "jade-server"]);

gulp.task("tsc",["tsc-client", "tsc-server"]);

gulp.task("vendorJs", function() {
    gulp.src(config.vendorJs)
        .pipe(plumber())
        .pipe(concat("vendor.js"))
        .pipe(gulp.dest("./build/client/js"))
});

gulp.task("sass", function() {
    gulp.src(["./source/client/sass/**/*.scss"])
        .pipe(plumber())
        .pipe(sourceMaps.init())
        .pipe(sass())
        .pipe(sourceMaps.write("."))
        .pipe(gulp.dest("./build/client/css"))
});

gulp.task("watch", function() {
    gulp.watch("./source/server/**/*.ts", ["tsc-server"]);
    gulp.watch("./source/client/**/*.ts", ["tsc-client"]);
    gulp.watch("./source/server/**/*.jade", ["jade-server"]);
    gulp.watch("./source/client/**/*.jade", ["jade-client"]);
    gulp.watch("./source/**/*.scss", ["sass"]);
});

gulp.task("copy", ["copyFonts"], function() {
    gulp.src(["./source/**/*.json"])
        .pipe(plumber())
        .pipe(gulp.dest("./build"));
})

gulp.task("copyFonts", function() {
    gulp.src(["./bower_components/bootstrap-sass/assets/fonts/bootstrap/glyphicons*"])
        .pipe(plumber())
        .pipe(gulp.dest("./build/client/fonts"))
});

gulp.task("default", ["watch", "vendorJs", "tsc", "sass", "jade", "copy"]);
